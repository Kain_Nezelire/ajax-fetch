class WebsiteSection extends HTMLElement {
    constructor() {
      super();
      this.attachShadow({ mode: 'open' });
    }
  
    static get observedAttributes() {
      return ['title', 'description'];
    }
  
    connectedCallback() {
      this.render();
    }
  
    attributeChangedCallback(name, oldValue, newValue) {
      this.render();
    }
  
    render() {
      const title = this.getAttribute('title') || '';
      const description = this.getAttribute('description') || '';
  
      this.shadowRoot.innerHTML = `
        <style>
          .app-section {
            background-color: rgba(255, 255, 255, 0.5); /* Прозрачный белый фон */
            border-radius: 10px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1); /* Тень */
          }
          .app-title,
          .app-subtitle {
            background-color: transparent; /* Прозрачный фон для заголовка и описания */
            margin: 0;
            padding: 0;
          }
        </style>
        <section class="app-section">
          <h2 class="app-title">${title}</h2>
          <h3 class="app-subtitle">${description}</h3>
          <div class="content">
            <slot></slot>
          </div>
        </section>
      `;
    }
  }
  
  customElements.define('website-section', WebsiteSection);
  