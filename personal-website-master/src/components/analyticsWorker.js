self.addEventListener('message', async (event) => {
  const { type, payload } = event.data;
  if (type === 'clickEvent') {
      processClickEvent(payload);
  }
});

let clickEventsBuffer = [];

async function processClickEvent(eventType) {
  clickEventsBuffer.push(eventType);
  if (clickEventsBuffer.length >= 5) {
      await sendBatchData(clickEventsBuffer);
      clickEventsBuffer = [];
  }
}

async function sendBatchData(data) {
  try {
      const response = await fetch('http://localhost:3000/analytics/user', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
      });
      if (!response.ok) {
          console.error('Failed to send batch data to server.');
      }
  } catch (error) {
      console.error('Error sending batch data:', error);
  }
}
