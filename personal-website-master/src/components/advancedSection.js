import { validate } from '../utils/validator.js';

class AdvancedSection {
    constructor() {
        this.joinProgram = null;
        this.inputEmail = null;
        this.buttonEmail = null;
    }

    create() {
        this.joinProgram = document.createElement('div');
        const headerDiv = document.createElement('div');
        const nameSection = document.createElement('h2');
        const headerDivParagraph = document.createElement('p');
        const formEmail = document.createElement('form');
        this.inputEmail = document.createElement('input');
        this.buttonEmail = document.createElement('button');

        const footer = document.querySelector(".app-footer");

        nameSection.textContent = "Join our Advanced Program";
        headerDivParagraph.textContent = "Sed do eiusmod tempor incidunt ut labore et dolore magna aliqua";
        this.buttonEmail.textContent = "Subscribe Advanced";
        this.inputEmail.placeholder = "Email";

        this.inputEmail.type = "email";

        this.joinProgram.className = "app-newsection__background";
        headerDiv.className = "app-newsection__header";
        formEmail.className = "app-newsection__form";

        this.joinProgram.appendChild(headerDiv);
        headerDiv.appendChild(nameSection);
        headerDiv.appendChild(headerDivParagraph);

        formEmail.appendChild(this.inputEmail);
        formEmail.appendChild(this.buttonEmail);
        this.joinProgram.append(formEmail);

        const beforeFooter = footer.previousElementSibling;
        beforeFooter.parentNode.insertBefore(this.joinProgram, beforeFooter);

        formEmail.addEventListener('submit', (event) => {
            event.preventDefault();

            const email = this.inputEmail.value;
            const isValid = validate(email);

            if (isValid) {
                alert('Email is valid. Subscribings...');
            } else {
                alert('Invalid email address. Please provide a valid email ending with gmail.com, outlook.com, or yandex.ru');
            }
        });

        this.inputEmail.addEventListener('focus', () => {
            this.inputEmail.placeholder = ''
        });

        this.inputEmail.addEventListener('blur', () => {
            if (!this.inputEmail.value) {
                this.inputEmail.placeholder = 'Email'
            }
        });
    }
}

export { AdvancedSection };
