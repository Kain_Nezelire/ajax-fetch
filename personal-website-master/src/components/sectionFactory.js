import { StandardSection } from './standartSection.js'; 
import { AdvancedSection } from './advancedSection.js';

class SectionFactory {
    static createSection(type) {
        switch (type) {
            case 'standard':
                return new StandardSection();
            case 'advanced':
                return new AdvancedSection();
            default:
                throw new Error('Unknown section type');
        }
    }
}

export { SectionFactory };