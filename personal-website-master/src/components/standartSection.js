import subscribeButtonFunction from '../utils/subscribeButton.js';
import { validate } from '../utils/validator.js';
import "../styles/style.css";



class StandardSection {
    constructor() {
        this.joinProgram = null;
        this.inputEmail = null;
        this.buttonEmail = null;
        this.analyticsWorker = new Worker('analyticsWorker.js');
    }

    create() {
        this.joinProgram = document.createElement('div');
        const headerDiv = document.createElement('div');
        const nameSection = document.createElement('h2');
        const headerDivParagraph = document.createElement('p');
        const formEmail = document.createElement('form');
        this.inputEmail = document.createElement('input');
        this.buttonEmail = document.createElement('button');

        const footer = document.querySelector(".app-footer");

        nameSection.textContent = "Join our Program";
        headerDivParagraph.textContent = "Sed do eiusmod tempor incidunt ut labore et dolore magna aliqua";
        this.buttonEmail.textContent = "Subscribe";
        this.inputEmail.placeholder = "Email";

        this.inputEmail.type = "email";

        this.joinProgram.className = "app-newsection__background";
        headerDiv.className = "app-newsection__header";
        formEmail.className = "app-newsection__form";

        this.joinProgram.appendChild(headerDiv);
        headerDiv.appendChild(nameSection);
        headerDiv.appendChild(headerDivParagraph);

        formEmail.appendChild(this.inputEmail);
        formEmail.appendChild(this.buttonEmail);
        this.joinProgram.append(formEmail);

        footer.parentNode.insertBefore(this.joinProgram, footer);

        const savedEmail = localStorage.getItem('subscriptionEmail');
        if (savedEmail) {
            this.inputEmail.value = savedEmail;
        }

        this.inputEmail.addEventListener('input', () => {
            localStorage.setItem('subscriptionEmail', this.inputEmail.value);
            this.sendClickEventToWorker('inputEvent');
        });

        this.buttonEmail.addEventListener('click',() => {
            if (this.buttonEmail.textContent === 'Subscribe') {
                this.subscribe();
                this.sendClickEventToWorker('buttonClick');
            } else {
                this.unsubscribe();
                this.sendClickEventToWorker('buttonClick');
            }
        });

        const isSubscribed = localStorage.getItem('subscribed');
        if (isSubscribed === 'true') {
            this.buttonEmail.textContent = 'Unsubscribe';
            this.inputEmail.style.display = 'none';
        }

        this.fetchCommunityInfo();
    }

    sendClickEventToWorker(eventType) {
        this.analyticsWorker.postMessage({ type: 'clickEvent', payload: eventType });
    }
    async subscribe() {
        if (!validate(this.inputEmail.value)) {
            window.alert('Please enter a valid email address.');
            return;
        }

        try {
            const response = await fetch('http://localhost:3000/subscribe', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: this.inputEmail.value })
            });

            if (response.status === 200) {
                localStorage.setItem('subscribed', 'true');
                this.buttonEmail.textContent = 'Unsubscribe';
                this.inputEmail.style.display = 'none';
            } else if (response.status === 422) {
                const responseData = await response.json();
                window.alert(responseData.error);
            }
        } catch (error) {
            console.error('Error:', error);
            window.alert('An error occurred. Please try again.');
        } finally {
            this.buttonEmail.disabled = false;
            this.buttonEmail.style.opacity = '1';
        }
    }

    async unsubscribe() {
        try {
            const response = await fetch('http://localhost:3000/unsubscribe', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if (response.ok) {
                localStorage.removeItem('subscribed');
                localStorage.removeItem('subscriptionEmail');
            }
            this.buttonEmail.textContent = 'Subscribe';
            this.inputEmail.style.display = 'block';
            this.inputEmail.value = '';
        } catch (error) {
            console.error('Error:', error);
            window.alert('An error occurred. Please try again.');
        } finally {
            this.buttonEmail.disabled = false;
            this.buttonEmail.style.opacity = '1';
        }
    }

    fetchCommunityInfo() {
        fetch('http://localhost:3000/community')
            .then(response => {
                if (response.ok) {
                } else {
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }
}

export { StandardSection };
