import { validate } from './validator.js';

function subscribeButtonFunction(inputEmail, buttonEmail) {
    buttonEmail.addEventListener('click', () => {
        if (buttonEmail.textContent === 'Subscribe') {
            const email = inputEmail.value;
            const isValid = validate(email);

            if (isValid) {
                const url = 'http://localhost:8080/subscribe';
                const data = { email: email };

                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    console.log(data);
                })
                .catch(error => {
                    console.error('There was a problem with your fetch operation:', error);
                });
            } else {
                alert('Please enter a valid email address.');
            }
        }
    });
}

export default subscribeButtonFunction;
