import { expect } from 'chai';
import { validate , validateEmailAsync} from '../utils/validator.js';

describe('Email Validation', () => {
  describe('validate()', () => {
    it('should return false for an empty email', () => {
      const result = validate('');
      expect(result).to.be.false;
    });

    it('should return false for an email without "@" and "."', () => {
      const result = validate('invalidemail');
      expect(result).to.be.false;
    });

    it('should return true for a valid email', () => {
      const result = validate('valid@email.com');
      expect(result).to.be.true;
    });
  });

  describe('validateEmailAsync()', () => {
    it('should return a Promise', async() => {
      const result = validateEmailAsync('');
      expect(result).to.be.an.instanceOf(Promise);
    });

    it('should resolve to true after a delay', async () => {
      return validateEmailAsync('mymail@mail.ru').then(result => {
        expect(result).to.be.true;
      });
    }); 
  });
});
