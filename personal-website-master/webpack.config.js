    const HtmlWebpackPlugin = require('html-webpack-plugin');
    const CopyWebpackPlugin = require('copy-webpack-plugin');
    const path = require('path');
    const TerserPlugin = require('terser-webpack-plugin');

    module.exports = {
        entry: {
            main: './src/components/main.js'
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './src/index.html'
            }),
            new CopyWebpackPlugin({
                patterns: [
                { from: "src/assets/images", to: "assets/images" },
                ],
            }),
        ],
        
        output:{
            'filename':'bundle.js'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(jpeg|jpg|gif|png)$/i,
                    type: "asset/resource",
                    generator: {
                    filename: "assets/images/[name][ext]",
                    },
                },
                {
                    test: /\.html$/,
                    use: 'html-loader',
                },
            ],
        },
        devServer: {
            port: 8080,
            hot: true,
            proxy:[{
                '/api': 'http://localhost:3000',
            }]
        },
        optimization: {
            minimize: true,
            minimizer: [new TerserPlugin()]
        }
    };
